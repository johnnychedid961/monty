import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import './App.css';

import PrivateRoute from './components/common/PrivateRoute';
import appRoutesBuilder, { appRoutes } from './routes/appRoutes';
import auth from './helpers/auth';
import Login from './routesComponents/Login/Login';
import MainApp from './routesComponents/MainApp/MainApp';

class App extends React.Component {

  getDefaultRedirect = () => auth.isAuthenticated() ? appRoutesBuilder.getAppUrl() : appRoutesBuilder.getLoginUrl();
  
  render (){
    return (
      <Router>
        <Switch>
          <Route path={appRoutes.LOGIN_URL}>
            <Login />
          </Route>

          <PrivateRoute path={appRoutes.APP_URL}>
            <MainApp />
          </PrivateRoute>

            <Redirect to={{ pathname: this.getDefaultRedirect() }} />
        </Switch> 
        <ToastContainer />
      </Router>
    )
  }

};

export default App;
