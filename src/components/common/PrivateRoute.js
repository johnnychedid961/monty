import React from 'react';
import { Route, Redirect } from "react-router-dom";

import appRoutesBuilder from '../../routes/appRoutes';
import auth from '../../helpers/auth';

function PrivateRoute({ children, ...rest }) {
    return (
        <Route
        {...rest}
        render={({ location }) =>
            auth.isAuthenticated() ? (
            children
            ) : (
            <Redirect
                to={{
                    pathname: appRoutesBuilder.getLoginUrl(),
                    state: { from: location }
                }}
            />
            )
        }
        />
    );
}
export default PrivateRoute;
  