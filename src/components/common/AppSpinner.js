import React from 'react';


const AppSpinner = ({isSpinning}) => {
  return (
    <>
      {
        isSpinning && 
        <div className="bg-white d-flex-center h-100 opacity-70 position-absolute w-100 z-1">
          <div className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      }
    </>
  );
}

export default AppSpinner;