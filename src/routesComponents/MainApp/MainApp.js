import React from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBBtn } from 'mdbreact';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';

import appRoutesBuilder, {appRoutes} from '../../routes/appRoutes';
import auth from '../../helpers/auth';
import Dashboard from '../Dashboard/Dashboard';
import Table from '../Table/Table';
import Users from '../Users/Users';
import AppSpinner from '../../components/common/AppSpinner';
import { delay } from '../../helpers/functions';

// IMPORTANT - used top bar since side navigation is a paid feature

class MainApp extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            collapse: false,
            isLoading: false
        };
    }

    onClick = () => this.setState({ collapse: !this.state.collapse})

    logout = () => {
        const {history} = this.props;
        this.setState({ isLoading: true })
        delay(1000).then( () => {
            auth.signout(() => {
              //No need to setState and remove loader since the comp will be unMounted using the history.replace()
              history.replace(appRoutesBuilder.getLoginUrl());
            });
        })
    };
    render(){

        return(
            <div>
                <AppSpinner isSpinning={this.state.isLoading}/>
                <header>
                    <MDBNavbar color="default-color" dark expand="md">
                        <MDBNavbarBrand href="/">
                            <strong>Monty</strong>
                        </MDBNavbarBrand>
                        <MDBNavbarToggler onClick={this.onClick} />
                        <MDBCollapse isOpen={this.state.collapse} navbar>
                            <MDBNavbarNav left>
                            <MDBNavItem>
                                <MDBNavLink to={appRoutesBuilder.getDashboardUrl()}>Dahboard</MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink to={appRoutesBuilder.getTableUrl()}>Table</MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink to={appRoutesBuilder.getUsersUrl()}>Users</MDBNavLink>
                            </MDBNavItem>
                            </MDBNavbarNav>
                            <MDBNavbarNav right>
                            <MDBNavItem onClick={this.logout}>
                                <MDBBtn size="sm">logout</MDBBtn>
                            </MDBNavItem>
                        </MDBNavbarNav>
                        </MDBCollapse>
                    </MDBNavbar>
                </header>
                
                <div className="p-3">
                    <Switch>
                        <Route path={appRoutes.DASHBOARD_URL} component={Dashboard} />
                        <Route path={appRoutes.TABLE_URL} component={Table} />
                        <Route path={appRoutes.USERS_URL} component={Users} />
                        <Redirect to={{ pathname: appRoutesBuilder.getDefaultLandingPage() }} />
                    </Switch>
                </div>
            </div>
        )
    }

}
export default withRouter(MainApp)