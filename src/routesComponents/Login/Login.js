import React from 'react';
import { withRouter  } from 'react-router-dom';
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';

import auth from '../../helpers/auth';
import appRoutesBuilder from '../../routes/appRoutes';
import AppSpinner from '../../components/common/AppSpinner';
import {delay} from '../../helpers/functions';


class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state= {
      isLoading: false
    }
    let { from } = this.props.location.state || { from: { pathname: appRoutesBuilder.getAppUrl() } };
    this._from = from;
  }

  login = () => {
    const { history } = this.props;
    this.setState({ isLoading: true })
    delay(1000).then( () => {
      auth.authenticate(() => {
        history.replace(this._from);
      });
    })
  };

  render () {
    if(auth.isAuthenticated())  this.props.history.replace(appRoutesBuilder.getDefaultLandingPage())

    return (
      <div className="full-screen d-flex-center">
        <MDBContainer>
          <MDBRow center>
            <MDBCol md="6">
              <AppSpinner isSpinning={this.state.isLoading}/>
              <form>
                <p className="h5 text-center mb-4">Sign in</p>
                <div className="grey-text">
                  <MDBInput label="Type your email" icon="envelope" group type="email" validate error="wrong"
                    success="right" />
                  <MDBInput label="Type your password" icon="lock" group type="password" validate />
                </div>
                <div className="text-center">
                  <MDBBtn onClick={this.login}>
                    Login
                  </MDBBtn>
                </div>
              </form>
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </div>
    );
  }
};

export default withRouter(Login)