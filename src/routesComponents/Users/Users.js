import React from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn } from 'mdbreact';
import { delay } from '../../helpers/functions';
import AppSpinner from '../../components/common/AppSpinner';
import { setLocalStorage, getLocalStorage } from '../../helpers/localStorage';

const LOCAL_STORAGE_KEY = "USER_FORM";
const FORM_INPUT_NAMES = {
    NAME        : "name",
    PASSWORD    : "password",
    ROLE        : "role",
}
export default class Users extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            [FORM_INPUT_NAMES.NAME]    : "",
            [FORM_INPUT_NAMES.PASSWORD]: "",
            [FORM_INPUT_NAMES.ROLE]    : -1,
            isLoading : false
        }
    }
    handleChange = e => this.setState({ [e.target.name]: e.target.value })
    onSubmit = () => {
        this.setState({ isLoading: true })
        //emulate save api call to the server
        const {isLoading, ...toStore} = this.state;
        delay(2000).then( () => {
            setLocalStorage(LOCAL_STORAGE_KEY, toStore);
            this.setState({ isLoading: false })
        })
    }

    componentDidMount() {
        //get data from local storage and fill the form
        const dataFromStore = getLocalStorage(LOCAL_STORAGE_KEY);
        this.setState({ ...this.state, ...dataFromStore });
    }
    render() {
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6">
                        <AppSpinner isSpinning={this.state.isLoading}/>
                        <form>
                            <div className="grey-text">
                                <label htmlFor="userFormName" className="grey-text">
                                    Your email
                                </label>
                                <input type="email" id="userFormName" className="form-control" name={FORM_INPUT_NAMES.NAME} value={this.state.name} onChange={this.handleChange}/>
                                <br />

                                <label htmlFor="userFormPass" className="grey-text">
                                    Your email
                                </label>
                                <input type="email" id="userFormPass" className="form-control" name={FORM_INPUT_NAMES.PASSWORD} value={this.state.password} onChange={this.handleChange}/>
                                <br />

                                <label htmlFor="userFormRole" className="grey-text">
                                    Role
                                </label>
                                <select id="userFormRole" className="browser-default custom-select" name={FORM_INPUT_NAMES.ROLE} value={this.state.role} onChange={this.handleChange}>
                                    <option disabled value="-1">Choose your option</option>
                                    <option value="1">Owner</option>
                                    <option value="2">Manager</option>
                                    <option value="3">Admin</option>
                                </select>
                            </div>
                            <div className="text-center mt-4">
                                <MDBBtn outline color="secondary" onClick={this.onSubmit}>
                                    Save
                                </MDBBtn>
                            </div>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        )
    }
}