import React from 'react';
import { MDBContainer, MDBRow, MDBCol } from 'mdbreact';
import { Line } from 'react-chartjs-2';

import messagesData from "./messagesData.json";
import callsData from "./callsData.json";

const dataList = [messagesData, callsData];
export default class Dashboard extends React.Component {

    render() {
        return (
            <MDBContainer>
                <MDBRow center>
                    <MDBCol md="10">
                        {
                            dataList.map( (data, i) => (
                                <div className="mt-5" key={i}>
                                    <Line data={data}/>
                                </div>
                            ))
                        }
                    </MDBCol>
                </MDBRow>
            </MDBContainer>

        )
    }
}