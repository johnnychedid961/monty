import React from 'react';
import MaterialTable from 'material-table';
import { MDBBtn, MDBModal, MDBModalHeader, MDBModalBody, MDBModalFooter } from 'mdbreact';
import { toast } from 'react-toastify';

const initialData = [
    { firstName: 'Mehmet', lastName: 'Baran', nationality: "Leb", phone: "9617889900", lastMonthBill:"1122", address: "here and there", age: 12 },
    { firstName: 'Mehmet1', lastName: 'Baran1', nationality: "Leb1", phone: "9617889901", lastMonthBill:"2122", address: "here", age: 22 },
    { firstName: 'Mehmet2', lastName: 'Baran2', nationality: "Leb2", phone: "9617889902", lastMonthBill:"1322", address: "there", age: 33 },
    { firstName: 'Mehmet3', lastName: 'Baran3', nationality: "Leb3", phone: "9617889903", lastMonthBill:"22", address: "there and here", age: 44 },
    { firstName: 'Mehmet4', lastName: 'Baran4', nationality: "Leb4", phone: "9617889904", lastMonthBill:"11", address: "over there", age: 27 },
]
export default class Table extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            data            : initialData,
            openModalRowData: {},
            isModalOpen     : false
        }
    }


    openDetailsModal = rowDetails => {
        this.setState({
            openModalRowData: rowDetails,
            isModalOpen     : true
        })
    }
    closeDetailsModal = () => {
        this.setState({
            openModalRowData: {},
            isModalOpen     : false
        })
    }

    render() {
        const {
            openModalRowData
        } = this.state;
        return (
            <div>
                <MaterialTable
                    editable={{
                        // There is a bug in the latest version for the add check https://github.com/mbrn/material-table/issues/2269 -- solution is to either downgrade or wait the fix patch
                        onRowAdd: newData => new Promise((resolve, reject) => {
                            setTimeout(() => {
                                this.setState({ data: [...this.state.data, newData] });
                                resolve();
                            }, 1000)
                        }),
                        onBulkEditRowChanged: () => {},
                        onRowUpdate: (newData, oldData) => new Promise((resolve, reject) => {
                            setTimeout(() => {
                                const dataUpdate = [...this.state.data];
                                const index = oldData.tableData.id;
                                dataUpdate[index] = newData;
                                this.setState({ data: [...dataUpdate] });
                                toast.success("Row edited successfully");
                                resolve();
                            }, 1000)
                        }),
                        onRowDelete: oldData => new Promise((resolve, reject) => {
                            setTimeout(() => {
                                const dataDelete = [...this.state.data];
                                const index = oldData.tableData.id;
                                dataDelete.splice(index, 1);
                                this.setState({ data: [...dataDelete] });
                                toast.success("Row deleted successfully");
                                resolve()
                            }, 1000)
                        }),
                    }}
                    columns={[
                        { title: 'First Name', field: 'firstName' },
                        { title: 'Last Name', field: 'lastName' },
                        { title: 'Nationality', field: 'nationality' },
                        { title: 'Phone', field: 'phone' },
                        { title: 'Action', render: rowData => <MDBBtn size="sm" onClick={ () => this.openDetailsModal(rowData)}>More</MDBBtn> }
                    ]}
                    data={this.state.data}
                    title="Users table"
                />

                <MDBModal isOpen={this.state.isModalOpen} toggle={this.closeDetailsModal}>
                    <MDBModalHeader toggle={this.toggle}>{openModalRowData.firstName} {openModalRowData.firstName}</MDBModalHeader>
                    <MDBModalBody>
                        <div>First Name: {openModalRowData.firstName}</div>
                        <div>Last Name: {openModalRowData.firstName}</div>
                        <div>Nationality: {openModalRowData.nationality}</div>
                        <div>Phone: {openModalRowData.firstName}</div>
                        <div>Last Month Bill: {openModalRowData.lastMonthBill}</div>
                        <div>Address: {openModalRowData.address}</div>
                        <div>Age: {openModalRowData.age}</div>
                    </MDBModalBody>
                    <MDBModalFooter>
                        <MDBBtn size="sm" onClick={this.closeDetailsModal}>Close</MDBBtn>
                    </MDBModalFooter>
                </MDBModal>

            </div>
        )
    }
}