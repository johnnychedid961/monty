
//helpers create to be able to replace localstorage with any other storage without having to replace it everywhere in the project
export const getLocalStorage = (key) => {
    let value = window.localStorage.getItem(key);
    if(value) value = JSON.parse(value);
    return value;
}
export const setLocalStorage = (key, value) => window.localStorage.setItem(key, JSON.stringify(value));
export const removeLocalStorage = (key) => window.localStorage.removeItem(key);
