
import C from './constances';
import { getCookie, deleteCookie, setCookie } from './cookies';
import {delay} from './functions';

const auth = {
    getAuthCookie() {
        return getCookie(C.COOKIE_KEYS.AUTH_TOKEN)
    },
    isAuthenticated() {
        return !!auth.getAuthCookie();
    } ,
    authenticate(cb) {
        setCookie(C.COOKIE_KEYS.AUTH_TOKEN, "123");
        delay(1000).then(cb); //emulate API call
    },
    signout(cb) {
        deleteCookie(C.COOKIE_KEYS.AUTH_TOKEN);
        delay(1000).then(cb); //emulate API call
    }
};

export default auth;