//Url used in the router to determine where each route should go
const BASE_URL  = ``;
export const appRoutes = {
    BASE_URL            : `${BASE_URL}`,
    LOGIN_URL           : `${BASE_URL}/login`,
    APP_URL             : `${BASE_URL}/app`,
    DASHBOARD_URL       : `${BASE_URL}/app/dashboard`,
    TABLE_URL           : `${BASE_URL}/app/table`,
    USERS_URL           : `${BASE_URL}/app/users`,
}

//Used to create specific routes with params to redirect to specific screen
const appRoutesBuilder    = {
    getDefaultLandingPage  : () => appRoutes.DASHBOARD_URL,

    getBaseUrl             : () => appRoutes.BASE_URL,
    getLoginUrl            : () => appRoutes.LOGIN_URL,
    getAppUrl              : () => appRoutes.APP_URL,
    getDashboardUrl        : () => appRoutes.DASHBOARD_URL,
    getTableUrl            : () => appRoutes.TABLE_URL,
    getUsersUrl            : () => appRoutes.USERS_URL,
}

export default appRoutesBuilder ;